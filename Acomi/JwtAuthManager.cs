﻿using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Acomi
{
    public class JwtAuthManager : IJwtAuthManager
    {
        private readonly IDictionary<string, Usuarios> _users = new Dictionary<string, Usuarios>();

        private readonly string key;


        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }

        public JwtAuthManager(string key)
        {
            this.key = key;
        }

        private KeyValuePair<string, Usuarios> ExisteUsuario(string correo)
        {
            return _users.FirstOrDefault(e => e.Value.Correo == correo);
        }

        public object Authenticate(string Correo, string Contrasena, AcomiContext context, string privateKey)
        {
            var existe = this.ExisteUsuario(Correo);

            if(existe.Key != null && existe.Value != null)
            {
                return new
                {
                    Usuario = new
                    {
                        id = existe.Value.Id,
                        codUsuario = existe.Value.CodUsuario,
                        nombreUsuario = existe.Value.NombreUsuario,
                        correo = existe.Value.Correo,
                        fechaActualizacion = existe.Value.FechaActualizacion
                    },
                    Token = existe.Key,
                    Mensaje = "Usuario ya se encuentra en sesion."
                };
            }

            var usuario = context.Usuarios.SingleOrDefault(x => x.Correo == Correo);

            // usuario no encontrado
            if (usuario == null)
                return null;

            var usuarioContrasenia = Helper.DecryptString(usuario.Contrasena, privateKey);
            if (usuarioContrasenia != Contrasena)
                return null;

            //if (!users.Any(e => e.Key == username && e.Value == password))
            //    return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.ASCII.GetBytes(key);
            var tokenDesc = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, Correo)
                }),
                Expires = DateTime.UtcNow.AddHours(12),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDesc);
            var tokenValue = tokenHandler.WriteToken(token);

            _users.Add(tokenValue, usuario);

            return new { 
                Usuario = new
                {
                    id = usuario.Id,
                    codUsuario = usuario.CodUsuario,
                    nombreUsuario = usuario.NombreUsuario,
                    correo = usuario.Correo,
                    fechaActualizacion = usuario.FechaActualizacion
                }, 
                Token = tokenValue
            };

            //return tokenHandler.WriteToken(token);
        }

        public Task<ClaimsPrincipal> UsuarioActual()
        {
            throw new NotImplementedException();
        }
    }
}