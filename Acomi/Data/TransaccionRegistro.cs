﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Acomi.Data
{
    public class TransaccionRegistro
    {
        [Required]
        public string CodCuenta { get; set; }

        [Required]
        public string CodCliente { get; set; }

        [Required]
        public string CodRuta { get; set; }

        //[Required]
        //public string NumeroCombrobante { get; set; }

        [Required]
        [Range(0, 1000000)]
        public decimal Importe { get; set; }

        [Required]
        [Range(0, 1)]
        public int CodTipoTransaccion { get; set; }

    }
}
