﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acomi.Models;
using Microsoft.EntityFrameworkCore;

namespace Acomi.Data
{
    public class AcomiContext: DbContext
    {
        public AcomiContext(DbContextOptions<AcomiContext> options) :base(options)
        {

        }

        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Cuentas> Cuentas { get; set; }
        public DbSet<Transacciones> Transacciones { get; set; }
        public DbSet<Usuarios> Usuarios { get; set; }
    }
}
