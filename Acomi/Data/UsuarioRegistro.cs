﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Acomi.Data
{
    public class UsuarioRegistro
    {
        [Required]
        [MaxLength(150)]
        [MinLength(4)]
        public string NombreUsuario { get; set; }

        [Required]
        [EmailAddress]
        public string Correo { get; set; }

        [Required]
        [MaxLength(150)]
        public string Contrasena { get; set; }
    }
}
