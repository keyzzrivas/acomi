﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Acomi.Data;
using Acomi.Models;

namespace Acomi
{
    public interface IJwtAuthManager
    {
        object Authenticate(string username, string password, AcomiContext context, string privateKey);
    }
}
