﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Acomi.Models
{
    [Table("cuentas")]
    public partial class Cuentas
    {
        public DateTime TimeStamp { get; set; }

        [Key]
        public int Id { get; set; }

        [ForeignKey("Clientes")]
        public int IdCarteraEjecutivo { get; set; }
        public string CodCuenta { get; set; }
        public string DescripcionProducto { get; set; }
        public string CodTipoCuenta { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
