﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Acomi.Models
{
    [Table("transacciones")]
    public partial class Transacciones
    {
        public DateTime TimeStamp { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CodMovimiento { get; set; }   
        public string CodCuenta { get; set; }
        public string CodCliente { get; set; }
        public string NombreCliente { get; set; }
        public string CodRuta { get; set; }
        public string CodEjecutivo { get; set; }
        public DateTime FechaCobro { get; set; }
        public string NumeroCombrobante { get; set; }
        public decimal Importe { get; set; }
        public string CodTransaccion { get; set; }
        public DateTime FechaInsert { get; set; }
        public string CodUsuario { get; set; }
        public string CodTipoCuenta { get; set; }
        public string Referencia { get; set; }
        public int IdMovimientoApp { get; set; }
        public DateTime FechaMovimientoApp { get; set; }
        public int IdClientePhoenix { get; set; }
        public int IdCuentaPhoenix { get; set; }
        public int CodTipoTransaccion { get; set; }
        public int IdTransacAnula { get; set; }
        public string UserToken { get; set; }
    }
}
