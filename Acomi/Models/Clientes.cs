﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Acomi.Models
{
    [Table("clientes")]
    public partial class Clientes
    {
        public DateTime TimeStamp { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string CodCliente { get; set; }

        [Required]
        public string NombreCliente { get; set; }

        [Required]
        public string CodRuta { get; set; }

        public bool Activo { get; set; }
        public string CodUsuarioCreo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string CodUsuarioActualizo { get; set; }
        public DateTime FechaActualizacion { get; set; }

        [ForeignKey("IdCarteraEjecutivo")]
        public virtual ICollection<Cuentas> Cuentas { get; set; }
    }
}
