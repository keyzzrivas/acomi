﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Acomi.Models
{
    [Table("usuarios")]
    public class Usuarios
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string CodUsuario { get; set; }

        [Required]
        public string NombreUsuario { get; set; }

        [Required]
        public string Correo { get; set; }

        [Required]
        public string Contrasena { get; set; }

        [Required]
        public DateTime FechaActualizacion { get; set; }
    }
}
