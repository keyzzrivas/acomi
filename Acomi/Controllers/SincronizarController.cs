﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

namespace Acomi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SincronizarController : ControllerBase
    {
        private readonly AcomiContext _context;
        private readonly IJwtAuthManager _jwtAuthManager;

        public SincronizarController(AcomiContext context, IJwtAuthManager jwtAuthManager)
        {
            _context = context;
            _jwtAuthManager = jwtAuthManager;
        }

        [HttpPost]
        public async Task<IActionResult> Registro([FromBody] TransaccionRegistro model)
        {
            try
            {
                var Cliente = await _context.Clientes.Where(e => e.CodCliente == model.CodCliente).FirstOrDefaultAsync();

                if (Cliente == null)
                    return NotFound(new { Error="Error al Sincronizar", Mensaje = "El Cliente no fue encontrado" });

                var Cuenta = await _context.Cuentas.Where(e => e.CodCuenta == model.CodCuenta).FirstOrDefaultAsync();

                if (Cuenta == null)
                    return NotFound(new { Error = "Error al Sincronizar", Mensaje = "El Numero de Cuenta no fue encontrado" });

                if (Cuenta.IdCarteraEjecutivo != Cliente.Id)
                    return BadRequest(new { Error = "Error al Sincronizar", Mensaje = "La cuenta "+Cuenta.CodCuenta+" no pertenece al cliente "+Cliente.CodCliente });

                // Obtiene el token de JWT
                var accessToken = Request.Headers[HeaderNames.Authorization];

                //Obtiene el usuario en session
                var claimsIdentity = this.User.Identity as ClaimsIdentity;
                var email = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
                var usuario =  await _context.Usuarios.FirstOrDefaultAsync(x => x.Correo == email);

                var count = await _context.Transacciones.CountAsync();
                var rnd = new Random();

                var Transac = new Transacciones
                {
                    CodMovimiento = "TRANS-" + count.ToString(),
                    CodCuenta = model.CodCuenta,
                    CodCliente = model.CodCliente,
                    NombreCliente = Cliente.NombreCliente,
                    CodRuta = model.CodRuta,
                    CodEjecutivo = usuario == null ? "" : usuario.Id.ToString(),
                    FechaCobro = DateTime.Now,
                    NumeroCombrobante = Guid.NewGuid().ToString(),
                    Importe = model.Importe,
                    CodTransaccion = "M-" + count.ToString() + "-" + rnd.Next(1000),
                    FechaInsert = DateTime.Now,
                    CodUsuario = usuario == null ? "" : usuario.CodUsuario.ToString(),
                    CodTipoCuenta = Cuenta.CodTipoCuenta,
                    Referencia = Guid.NewGuid().ToString(),
                    IdMovimientoApp = count,
                    FechaMovimientoApp = DateTime.Now,
                    IdClientePhoenix = Cliente.Id,
                    IdCuentaPhoenix = Cuenta.Id,
                    CodTipoTransaccion = model.CodTipoTransaccion,
                    IdTransacAnula = 0,
                    UserToken = accessToken
                };

                _context.Add(Transac);
                _context.SaveChanges();

                return Ok(new { Mensaje = "Sincronizacion Correcta", Transaccion = Transac });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
