﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.IdentityModel.Tokens;

namespace Acomi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly string _privateKey = "E546C8DF278CD5931069B522E695D4F2";
        private readonly AcomiContext _context;
        private readonly IJwtAuthManager _jwtAuthManager;

        public UsuariosController(AcomiContext context, IJwtAuthManager jwtAuthManager)
        {
            _context = context;
            _jwtAuthManager = jwtAuthManager;
        }

        [AllowAnonymous]
        [HttpPost("registro")]
        public IActionResult Registro([FromBody] UsuarioRegistro model)
        {
            try
            {
                var rnd = new Random();

                _context.Add(new Usuarios {
                    CodUsuario = "USER-"+ rnd.Next(1000),
                    NombreUsuario = model.NombreUsuario,
                    Correo = model.Correo,
                    Contrasena = Helper.EncryptString(model.Contrasena, _privateKey),
                    FechaActualizacion = DateTime.Now
                });

                _context.SaveChanges();
                return Ok("Usuario almacenado con exito!");
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("autenticar")]
        public IActionResult authenticate([FromBody] UsuarioLogin usuarioLogin)
        {
            try
            {

                var token = _jwtAuthManager.Authenticate(usuarioLogin.Correo, usuarioLogin.Contrasena, _context, _privateKey);
                if (token == null)
                    return Unauthorized(new { Error = "No Autorizado",  Mensaje = "Credenciales incorrectas" });

                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
