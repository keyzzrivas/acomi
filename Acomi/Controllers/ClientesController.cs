﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Authorization;

namespace Acomi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly AcomiContext _context;
        private readonly string _notFound = "No se encontraron coincidencias";

        public ClientesController(AcomiContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Clientes>> ObtenerClientes(int id)
        {
            var clientes = await _context.Clientes.FindAsync(id);

            if (clientes == null)
            {
                return NotFound(_notFound);
            }

            return clientes;
        }

        [HttpPost("Editar/{id}")]
        public async Task<IActionResult> EditarClientes(int id, Clientes clientes)
        {
            if (id != clientes.Id)
            {
                return BadRequest();
            }

            _context.Entry(clientes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientesExists(id))
                {
                    return NotFound(_notFound);
                }
                else
                {
                    throw;
                }
            }

            return Ok("Cliente actualizado con exito");
        }

        [HttpPost("Nuevo")]
        public async Task<ActionResult<Clientes>> CrearClientes(Clientes clientes)
        {
            _context.Clientes.Add(clientes);
            await _context.SaveChangesAsync();

            return clientes;
        }

        [HttpPost("Eliminar/{id}")]
        public async Task<ActionResult<Clientes>> EliminarClientes(int id)
        {
            var clientes = await _context.Clientes.FindAsync(id);
            if (clientes == null)
            {
                return NotFound();
            }

            _context.Clientes.Remove(clientes);
            await _context.SaveChangesAsync();

            return clientes;
        }

        private bool ClientesExists(int id)
        {
            return _context.Clientes.Any(e => e.Id == id);
        }
    }
}
