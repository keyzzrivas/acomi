﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Acomi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MostrarCuentasPorClienteController : ControllerBase
    {
        private readonly AcomiContext _context;
        private readonly string _notFound = "No se encontraron coincidencias";

        public MostrarCuentasPorClienteController(AcomiContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<List<Clientes>>> ObtenerClientesyCuentas(int id)
        {
            var result = await _context.Clientes.Where(x=> x.Id == id).Include("Cuentas").ToListAsync();

            if (result == null)
                return NotFound(_notFound);

            return result;
        }
    }
}
