﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Acomi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ListaClientesyCuentasController : ControllerBase
    {
        private readonly AcomiContext _context;
        private readonly string _notFound = "No se encontraron coincidencias";

        public ListaClientesyCuentasController(AcomiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<Clientes>>> ObtenerClientesyCuentas()
        {
            var result = await _context.Clientes.Include("Cuentas").ToListAsync();

            if (result == null)
                return NotFound(_notFound);

            return result;
        }
    }
}
