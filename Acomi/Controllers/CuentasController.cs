﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Acomi.Data;
using Acomi.Models;
using Microsoft.AspNetCore.Authorization;

namespace Acomi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        private readonly AcomiContext _context;
        private readonly string _notFound = "No se encontraron coincidencias";

        public CuentasController(AcomiContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Cuentas>> ObtenerCuentas(int id)
        {
            var cuentas = await _context.Cuentas.FindAsync(id);

            if (cuentas == null)
            {
                return NotFound(_notFound);
            }

            return cuentas;
        }

        [HttpPost("Editar/{id}")]
        public async Task<IActionResult> EditarCuentas(int id, Cuentas cuentas)
        {
            if (id != cuentas.Id)
            {
                return BadRequest();
            }

            _context.Entry(cuentas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CuentasExists(id))
                {
                    return NotFound(_notFound);
                }
                else
                {
                    throw;
                }
            }

            return Ok("Cuenta actualizada con exito");
        }

        [HttpPost("Nuevo")]
        public async Task<ActionResult<Cuentas>> CrearCuentas(Cuentas cuentas)
        {
            _context.Cuentas.Add(cuentas);
            await _context.SaveChangesAsync();

            return cuentas;
        }

        [HttpPost("Eliminar/{id}")]
        public async Task<ActionResult<Cuentas>> EliminarCuentas(int id)
        {
            var cuentas = await _context.Cuentas.FindAsync(id);
            if (cuentas == null)
            {
                return NotFound(_notFound);
            }

            _context.Cuentas.Remove(cuentas);
            await _context.SaveChangesAsync();

            return cuentas;
        }

        private bool CuentasExists(int id)
        {
            return _context.Cuentas.Any(e => e.Id == id);
        }
    }
}
